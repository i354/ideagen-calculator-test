using Ideagen_Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Calculator_Test
{
    [TestClass]
    public class CalculatorTest
    {
        [DataTestMethod]
        [DataRow("10 - 2", 8)]
        [DataRow("45 / 5", 9)]
        [DataRow("71 - 25", 46)]
        public void Calculate_Basic_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("1 + 1 * 3", 4)]
        [DataRow("83 + 12 * 5 - 10", 133)]
        [DataRow("19 * 10 / 5 + 34 - 4 * 2", 64)]
        [DataRow("6 * 6 / 6 + 6 - 6 * 6 + 6", -18)]
        public void Calculate_MixedOperatorWithoutParenthesis_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("( 11.5 + 15.4 ) + 10.1", 37)]
        [DataRow("10 - ( 2 - 3 )", 11)]
        [DataRow("( 1 / 2 ) - 1 + 1", 0.5)]
        [DataRow("9 / 3 + ( 5 - 2 ) + 2", 8)]
        public void Calculate_MixedOperatorWithSingleParenthesis_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("10 - ( 2 + 3 * ( 7 - 5 ) )", 2)]
        [DataRow("( 5 + 6 - 20 / 4 ) + ( 1 + 4 - ( 2 * 5 + ( 6 * 6 ) ) )", -35)]
        public void Calculate_MixedOperatorWithNestedParenthesis_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("6 ( 6 )", 36)]
        [DataRow("( 8 ) ( 6 )", 48)]
        [DataRow("( 2 ) ( 3 ) ( 2 ( 2 ) )", 24)]
        public void Calculate_ParenthesisOnlyWithNoOperators_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("1 + 6 / ( 6 )", 2)]
        [DataRow("( 8 ) * ( 2 + 2 ) ( 3 )", 96)]
        [DataRow("( 2 - 1 ) / ( 1 + 2 ) ( 2 ( 2 ) )", 1.33)]
        public void Calculate_ParenthesisWithOperators_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("6", 6)]
        [DataRow("- 6", -6)]
        [DataRow("+ 6", 6)]
        [DataRow("( 6 )", 6)]
        [DataRow("- ( 6 )", -6)]
        [DataRow("- ( - 6 )", 6)]
        [DataRow("( ( ( 6 ) ) )", 6)]
        [DataRow("- ( - ( - ( - 6 ) ) )", 6)]
        public void Calculate_SingleNumber_ReturnsCorrectCalculation(string sum, double expected)
        {
            var result = Calculator.Calculate(sum);
            Assert.AreEqual(result, expected);
        }

        [DataTestMethod]
        [DataRow("45 / 0")]
        [DataRow("1 / ( 0 )")]
        [DataRow("0 / ( ( 0 ) )")]
        [ExpectedException(typeof(DivideByZeroException))]
        public void Calculate_DivideByZero_ThrowsDivideByZeroException(string sum)
        {
            Calculator.Calculate(sum);
        }

        [DataTestMethod]
        [DataRow("45 ( ) 5")]
        [DataRow("( ( 5 )")]
        [DataRow("71 - + 8.25")]
        [DataRow("7 +")]
        [ExpectedException(typeof(InvalidOperationException), "Invalid operator placement.")]
        public void Calculate_InvalidOperatorsPlacement_ThrowsInvalidOperationException(string sum)
        {
            Calculator.Calculate(sum);
        }

        [DataTestMethod]
        [DataRow("6 9")]
        [DataRow("1 23 1")]
        [ExpectedException(typeof(InvalidOperationException), "Missing operator.")]
        public void Calculate_MissingOperator_ThrowsInvalidOperationException(string sum)
        {
            Calculator.Calculate(sum);
        }

        [DataTestMethod]
        [DataRow("45x 5")]
        [DataRow("71 @ 8.25")]
        [DataRow("45*5")]
        [ExpectedException(typeof(InvalidOperationException), "Input is invalid. Must not be empty and consists of numbers and operators + - * / only seperated by space.")]
        public void Calculate_InvalidInput_ThrowsInvalidOperationException(string sum)
        {
            Calculator.Calculate(sum);
        }
    }
}
