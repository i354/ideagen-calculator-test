﻿using System;

namespace Ideagen_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Ideagen Calculator Software...");
            Console.WriteLine("Input the string you wan to calculate here, type \"exit\" to terminate...");
            while (true)
            {
                Console.Write("Your input >> ");
                var input = Console.ReadLine();
                if (input.Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }

                try
                {
                    var answer = Calculator.Calculate(input);
                    Console.WriteLine($"Answer: {answer} \n");
                } 
                catch (InvalidOperationException ex)
                {
                    Console.WriteLine($"Error: {ex.Message} \n");
                } 
                catch (DivideByZeroException ex)
                {
                    Console.WriteLine($"Error: {ex.Message} \n");
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine($"Error: {ex.Message} \n");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message} \n");
                }
            }
            
        }
    }
}

