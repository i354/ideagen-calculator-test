﻿using Ideagen_Calculator.Constants;
using Ideagen_Calculator.Infra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ideagen_Calculator
{
    public class Calculator
    {
        public static double Calculate(string sum)
        {
            var isValidInput = InputHelper.ValidateInput(sum);
            if (!isValidInput)
            {
                throw new InvalidOperationException("Invalid input, parenthesis is not balanced.");
            }

            var numArray = InputHelper.SanitizeInput(sum).ToList();
            var length = numArray.Count();
            for (var i = length - 1; i >= 0; i--)
            {
                if (numArray[i] == Operator.LeftParenthesis)
                {
                    var j = i + 1;
                    while (numArray[j] != Operator.RightParenthesis)
                    {
                        j = ProcessMultiplicationDivision(ref numArray, j, true);
                    }

                    j = i + 1;
                    while (numArray[j] != Operator.RightParenthesis)
                    {
                        j = ProcessAdditionSubtraction(ref numArray, j, true);
                    }

                    // remove paranthesis
                    numArray.RemoveAt(i);
                    numArray.RemoveAt(j - 1);
                }
            }

            length = numArray.Count();
            var startIndex = 0;
            while (startIndex < length)
            {
                startIndex = ProcessMultiplicationDivision(ref numArray, startIndex);
                length = numArray.Count();
            }

            length = numArray.Count();
            startIndex = 0;
            while (startIndex < length)
            {
                startIndex = ProcessAdditionSubtraction(ref numArray, startIndex);
                length = numArray.Count();
            }

            var finalCount = numArray.Count();
            if (finalCount == 1)
            {
                var remainingNumber = numArray.FirstOrDefault();
                if (NumberHelper.IsDoubleParsable(remainingNumber)) {
                    var answer = NumberHelper.ParseDouble(remainingNumber);
                    var finalAnswerWithRounding = Math.Round(answer, 2);
                    return finalAnswerWithRounding;
                }
            }

            throw new InvalidOperationException("Missing operator.");
        }

        private static int ProcessMultiplicationDivision(ref List<string> numArray, int j, bool isParenthesis = false)
        {
            var isMultiplication = numArray[j] == Operator.Multiplication;
            var isDivision = numArray[j] == Operator.Division;
            if (isMultiplication || isDivision)
            {
                var number1 = NumberHelper.ParseDouble(numArray[j - 1]);
                var number2 = NumberHelper.ParseDouble(numArray[j + 1]);
                double result;
                if (isMultiplication)
                {
                    result = number1 * number2;
                } else
                {
                    if (number2 > 0)
                    {
                        result = number1 / number2;
                    }
                    else
                    {
                        throw new DivideByZeroException();
                    }
                }
                numArray.RemoveRange(j - 1, 3);
                numArray.Insert(j - 1, result.ToString());
            }
            else
            {
                j++;
            }

            return j;
        }

        private static int ProcessAdditionSubtraction(ref List<string> numArray, int j, bool isParenthesis = false)
        {
            var isAddition = numArray[j] == Operator.Addition;
            var isSubtraction = numArray[j] == Operator.Subtraction;
            if (isAddition || isSubtraction)
            {
                var number1 = NumberHelper.ParseDouble(numArray[j - 1]);
                var number2 = NumberHelper.ParseDouble(numArray[j + 1]);
                var result = isAddition ? number1 + number2 : number1 - number2;
                numArray.RemoveRange(j - 1, 3);
                numArray.Insert(j - 1, result.ToString());
            }
            else
            {
                j++;
            }

            return j;
        }
    }
}
