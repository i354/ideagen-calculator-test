﻿namespace Ideagen_Calculator.Constants
{
    public static class Operator
    {
        public const string LeftParenthesis = "(";
        public const string RightParenthesis = ")";
        public const string Addition = "+";
        public const string Subtraction = "-";
        public const string Multiplication = "*";
        public const string Division = "/";
    }
}
