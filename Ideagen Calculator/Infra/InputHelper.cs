﻿using Ideagen_Calculator.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ideagen_Calculator.Infra
{
    public class InputHelper
    {
        private static string prefixZero = "0";
        private static string CalculatorPattern = @"^[0-9 \+\-\*\/\(\).]+$";
        public static IList<string> SanitizeInput(string sum)
        {
            var numArray = sum.Split(' ').ToList();
            var length = numArray.Count();
            for (var i = 0; i < length; i++)
            {
                // handle prefix operator like 5 + ( + 4 ) -> 5 + ( 0 + 4 )
                if (IsAdditionSubtraction(numArray[i]))
                {
                    if (i == 0)
                    {
                        numArray.Insert(i, prefixZero);
                    }
                    else if (i - 1 >= 0 && !NumberHelper.IsDoubleParsable(numArray[i - 1]) && numArray[i - 1] != Operator.RightParenthesis)
                    {
                        numArray.Insert(i, prefixZero);
                    }
                }
                // handle invalid * and / operator, e.g. 3 + ( / 2 )
                else if (IsMultiplicationDivision(numArray[i]))
                {
                    if (i - 1 >= 0 && !NumberHelper.IsDoubleParsable(numArray[i - 1]) && numArray[i - 1] != Operator.RightParenthesis)
                    {
                        throw new InvalidOperationException("Invalid operator placement.");
                    }
                }
                /* to add * operator if the string use bracket to multiple, e.g.
                 *  5 ( 6 + 5 ) -> 5 * ( 6 + 5 )
                 *  ( 6 ) ( 2 ) - > ( 6 ) * ( 2 )
                 */
                else if (i + 1 < length && i + 2 < length && (NumberHelper.IsDoubleParsable(numArray[i]) || numArray[i] == Operator.RightParenthesis)
                    && numArray[i + 1] == Operator.LeftParenthesis && numArray[i + 1] != Operator.RightParenthesis)
                {
                    numArray.Insert(i + 1, Operator.Multiplication);
                }
            }

            return numArray;
        }

        public static bool ValidateInput(string sum)
        {
            if (!Regex.IsMatch(sum, CalculatorPattern))
            {
                throw new InvalidOperationException("Input is invalid. Must not be empty and consists of numbers and operators + - * / only seperated by space.");
            }

            if (!IsValidOperatorPlacement(sum))
            {
                throw new InvalidOperationException("Invalid operator placement.");
            }

            return true;
        }

        public static bool IsBalancedParanthesis(IList<string> numArray)
        {
            var length = numArray.Count();
            var parenthesisStack = new Stack<string>();
            for (var i = 0; i < length; i++)
            {
                if (numArray[i] == Operator.LeftParenthesis)
                {
                    parenthesisStack.Push(Operator.LeftParenthesis);
                } 
                else if (numArray[i] == Operator.RightParenthesis)
                {
                    parenthesisStack.Pop();
                }
            }

            return parenthesisStack.Count() == 0;
        }

        private static bool IsValidOperatorPlacement(string sum)
        {
            var numArray = sum.Split(' ').ToList();
            var length = numArray.Count();
            if (IsMultiplicationDivision(numArray[0]) || IsOperator(numArray[length - 1]))
            {
                return false;
            }

            for (var i = 0; i < length - 1; i++)
            {
                if (IsOperator(numArray[i]))
                {
                    // do not allow multiple operators sequence like + + 5, - - + 2, + - 1
                    if (IsOperator(numArray[i + 1]))
                    {
                        return false;
                    }
                }
                else if (numArray[i] == Operator.LeftParenthesis && numArray[i + 1] == Operator.RightParenthesis)
                {
                    return false;
                }
            }

            if (!IsBalancedParanthesis(numArray))
            {
                return false;
            }

            return true;
        }

        private static bool IsOperator(string sum)
        {
            return IsAdditionSubtraction(sum) || IsMultiplicationDivision(sum);
        }

        private static bool IsAdditionSubtraction(string sum)
        {
            return sum == Operator.Addition || sum == Operator.Subtraction;
        }

        private static bool IsMultiplicationDivision(string sum)
        {
            return sum == Operator.Multiplication || sum == Operator.Division;
        }
    }
}
