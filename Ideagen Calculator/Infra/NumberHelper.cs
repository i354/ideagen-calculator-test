﻿using System;

namespace Ideagen_Calculator.Infra
{
    public class NumberHelper
    {
        public static double ParseDouble(string inputNumber)
        {
            double number;
            if (Double.TryParse(inputNumber, out number))
            {
                return number;
            }

            return 0;
        }

        public static bool IsDoubleParsable(string inputNumber)
        {
            double number;
            if (Double.TryParse(inputNumber, out number))
            {
                return true;
            }

            return false;
        }
    }
}
