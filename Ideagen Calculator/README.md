# Ideagen Calculator Software
Welcome to ideagen calculator which operates on pure c# code without using any third party library.

## Time consumed
4 hours of thinking and drafting the initial solution
2 hours to optimize and restrcuture the code
4 hours to add test cases and write documentation 

## Solution Summary
1) First step will be validating the inputs to only allow numbers & operators like + - * /.(), and make sure all are seperated by spaces. The input will be split by space into an array for pcocessing.
2) Invalid operator placement like a division in front of a number ( / 4 ), unbalanced parenthesis, multiple operators stacked together ( + + 5 ) are also not allowed. 
3) Next, look for number with preceding operator (e.g. + 5, - 5) and add a 0 in front to make the calculation process easier later on ( to become 0 + 5, 0 - 5 )
4) To handle the calculation, all the parenthesis need to be handled and remove first, starting from the inner most parenthesis. To find that, loop backwards from the list and find for left parenthesis ( "(" ). Once found, we will get an index _i_  and proceed to process the operation within the parenthesis. 
5) Loop the list start from _i_ and look for multiplication or division operator ( *, / ). Once found, get the value in front of the operator (_i-1_) and after the operator (_i+1_) to perform the calculation. Next, replace the string starting from in front of the operator to after the operator to the answer obtained from the calculation.
6) Repeat step 5, but look for addition and subtraction operator instead ( +, - ).
7) Loop step 5 and 6 until the beginning of the list. Each loop will be removing one balanced parenthesis.
8) Now we have a clean string without parenthesis. Loop through the string to look for multiplication and division again ( * , / ). If found, proceed with the calculation by getting the value in front and back of the operator. Once done, replace the string with the answer obtained.
9) Repeat step 8 but look for addition and subtraction ( +, - ).
10) There will be one number left in the list which will be the answer of the calculation. If there are more than one number in the list, error will be return.

## How to build
1) Open `Ideagen Calculator.sln` in Visual Studio.
2) Click on "Build", then "Build Ideagen Calculator".

## How to test
1) Click on `Calculator Test` project.
2) Right click `CalculatorTest.cs` and click Run Test.

## How to publish (to folder)
1) Click on "Build", select "Publish Ideagen Calculator".
2) If there is no profile available, click on "Start", then select Folder. The default folder path will be `bin\Release\netcoreapp3.1\publish\`. Click "Create Profile".
3) Click on "Publish". Your file will be available in the path in step 2.

## How to run
1) Go to `bin\Release\netcoreapp3.1\publish` and click `Ideagen Calculator.exe`.

## How to use
1) You will be prompted a welcome message and there will be a new line stated "Your Input".
2) Type your input and hit "enter".
3) You will get your desired answer. If there is error, it will be shown as a message start with "Error: ...".
4) Type "exit" to terminate.
